﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace MainMenu
{
    public class Menu : MonoBehaviour
    {
        public void GoToHauntedHouse()
        {
            SceneManager.LoadScene(1);
        }

        public void GoToRollABall()
        {
            SceneManager.LoadScene(2);
        }

        public void GoToAirHockey()
        {
            SceneManager.LoadScene(3);
        }
    }
}