﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RollABall
{
    public class Rotator : MonoBehaviour
    {
        public float Speed = 3f;
        private void Update()
        {
            transform.Rotate(new Vector3(15, 30, 45) * Time.deltaTime * Speed);
        }
    }
}