﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace RollABall
{
    public class BallMovement : MonoBehaviour
    {
        public float Speed = 10f;
        public Text CountText;
        public GameState GameStateObject;

        private const int MAX_COUNT = 12;

        private Rigidbody myRigidbody;
        private int mCount;

        private void Start()
        {
            myRigidbody = GetComponent<Rigidbody>();
            mCount = 0;
            SetCountText();
        }
        private void FixedUpdate()
        {
            if (GameStateObject.LevelStartFinish())
            {
                float horizontal = Input.GetAxis("Horizontal");
                float vertical = Input.GetAxis("Vertical");
                Vector3 movement = new Vector3(horizontal, 0f, vertical);
                myRigidbody.AddForce(movement * Speed);
            }
        }
        private void OnTriggerEnter(Collider other)
        {
            if(other.CompareTag("PickUp"))
            {
                other.gameObject.SetActive(false);
                mCount++;
                SetCountText();
                if (mCount >= MAX_COUNT)
                {
                    mCount = 0;
                    GameStateObject.GameLevelUp();
                    GameStateObject.EndTheGame();
                }
            }
        }
        void SetCountText()
        {
            CountText.text = "Count : " + mCount.ToString();
        }

        
    }
}