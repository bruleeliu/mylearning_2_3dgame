﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace RollABall
{
    public class GameState : MonoBehaviour
    {
        #region public variable
        public int GameLevel = 1;
        public float FadeDuration = 1f;
        public float DisplayImageDuration = 1f;
        public CanvasGroup YouWinBackgroundCanvasGroup;
        public CanvasGroup LevelDisplayBackgroundCanvasGroup;
        public Text TimerText;
        public Text LevelText;
        public Text BestTimeText;
        public Transform Player;
        public Transform[] PickUpsArray;
        #endregion

        #region private variable
        bool mIsGameRestarting;
        bool mIsGameFadeInFinish;
        float mFadeInTimer;
        bool mIsLevelEnding;
        float mEndLevelTimer;
        private float mTimerCount; // current level time
        private int mTimerCountSec; // current level time
        private int mTimerCountMinute; // current level time
        private int mTimerCountHour; // current level time
        private int mBestLevelTime;
        #endregion

        #region private function
        private void Start()
        {
            mBestLevelTime = 0;
            RestartGame();
        }

        private void Update()
        {
            if (mIsGameRestarting)
                return;
            if (!mIsGameFadeInFinish)
            {
                DoGameFadeIn();
            }
            else
            {                    
                if (mIsLevelEnding)
                {
                    EndLevel();
                }
                else
                {
                    TimerCounter(false);
                }
            }
        }
        void RestartGame()
        {
            mIsGameRestarting = true; // start to reset game state
            LevelText.text = "Level " + GameLevel.ToString();
            LevelDisplayBackgroundCanvasGroup.alpha = 1;
            YouWinBackgroundCanvasGroup.alpha = 0;
            mIsGameFadeInFinish = false;
            mFadeInTimer = FadeDuration + DisplayImageDuration;
            mIsLevelEnding = false;
            Player.GetComponent<Rigidbody>().velocity = Vector3.zero;
            Player.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
            Player.position = new Vector3(0, 0.5f, 0);
            SetBestTime();
            if(GameLevel > 1)
                ResetPickUpsLocation();
            mIsGameRestarting = false; // finish to reset game state
        }
        void ResetPickUpsLocation()
        {
            List<Vector3> pickUpPossibleLoc = new List<Vector3>();
            pickUpPossibleLoc.Clear();
            for (int column = -9; column <= 9; column++)
                for (int row = -9; row <= 9; row++)
                {
                    if (column > -5 && column < 5 && row > -5 && row < 5)
                        continue;
                    pickUpPossibleLoc.Add(new Vector3(column, 0.5f, row));
                }
            for (int i = 0; i < PickUpsArray.Length; i++)
            {
                int newLocIndex = Random.Range(0, pickUpPossibleLoc.Count);
                PickUpsArray[i].position = pickUpPossibleLoc[newLocIndex];
                PickUpsArray[i].gameObject.SetActive(true);
                pickUpPossibleLoc.RemoveAt(newLocIndex);
            }
        }
        void DoGameFadeIn()
        {
            mFadeInTimer -= Time.deltaTime;
            LevelDisplayBackgroundCanvasGroup.alpha = mFadeInTimer / FadeDuration;
            if(mFadeInTimer < 0)
            {
                TimerCounter(true);
                mIsGameFadeInFinish = true;
            }
        }
        void EndLevel()
        {
            mEndLevelTimer += Time.deltaTime;
            YouWinBackgroundCanvasGroup.alpha = mEndLevelTimer / FadeDuration;
            if (mEndLevelTimer > FadeDuration + DisplayImageDuration)
            {
                RestartGame();
                //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
        }
        void SetBestTime()
        {
            int currentTime = mTimerCountSec + mTimerCountMinute * 60 + mTimerCountHour * 60 * 60;
            if (mBestLevelTime == 0)
                mBestLevelTime = currentTime;
            if(mBestLevelTime > currentTime)
                mBestLevelTime = currentTime;
            int sec = mBestLevelTime % 60;
            int minute = (mBestLevelTime / 60) % 60;
            int hour = (mBestLevelTime / 60 / 60) % 60;
            BestTimeText.text = "Best Time : " + hour.ToString("D2") + ":" + minute.ToString("D2") + ":" + sec.ToString("D2");
        }
        void TimerCounter(bool resetTimer)
        {
            if (resetTimer)
            {
                mTimerCount = 0f;
                mTimerCountSec = 0;
                mTimerCountMinute = 0;
                mTimerCountHour = 0;
            }
            mTimerCount += Time.deltaTime;
            if (mTimerCount >= 1f)
            {
                mTimerCount = 0f;
                mTimerCountSec++;
            }
            if (mTimerCountSec >= 60)
            {
                mTimerCountSec = 0;
                mTimerCountMinute++;
            }
            if (mTimerCountMinute >= 60)
            {
                mTimerCountMinute = 0;
                mTimerCountHour++;
            }
            TimerText.text = "Time : " + mTimerCountHour.ToString("D2") + ":" + mTimerCountMinute.ToString("D2") + ":" + mTimerCountSec.ToString("D2");
        }
        #endregion

        #region public function
        public bool LevelStartFinish()
        {
            return mIsGameFadeInFinish;
        }
        public void EndTheGame()
        {
            mIsLevelEnding = true;
        }
        public void GameLevelUp()
        {
            GameLevel += 1;
        }
        #endregion        
    }
}