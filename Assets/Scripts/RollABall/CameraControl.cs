﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RollABall
{
    public class CameraControl : MonoBehaviour
    {
        public Transform mTarget;
        private Vector3 mOffset;
        private void Start()
        {
            mOffset = transform.position - mTarget.position;
        }

        private void LateUpdate()
        {
            transform.position = mTarget.position + mOffset;
        }
    }
}