﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AirHockey
{
    public class BallMovement : MonoBehaviour
    {
        public float Speed = 4f;
        public GameState GameStateObject;

        #region for testing
        public float Xdir;
        public float Zdir;
        #endregion

        private Rigidbody mRigidbody;
        private Vector3 mStartVelocity;
        private bool mIsStartShoot;

        private void Start()
        {
            mRigidbody = GetComponent<Rigidbody>();
            mIsStartShoot = false;
        }
        private void FixedUpdate()
        {
            if(!mIsStartShoot)
                if(GameStateObject.IsStartingGame())
                {
                    mIsStartShoot = true;
                    Vector3 temp = Vector3.zero;
                    temp.x = Random.Range(-Speed, Speed);
                    temp.y = 0.6f;
                    temp.z = Random.Range(-Speed, Speed);
                    temp.Normalize();
                    mRigidbody.velocity = temp * Speed;
                    mRigidbody.AddForce(temp * Speed, ForceMode.Impulse);
                }
        }
        private void OnTriggerEnter(Collider other)
        {
            if(other.CompareTag("Goal"))
            {
                if (other.transform.position.z > 0)
                    GameStateObject.AddEnemyScore();
                else
                    GameStateObject.AddPlayerScore();
                ResetBall();
            }
            if(other.CompareTag("Wall"))
            {
                Vector3 ballVelocity = mRigidbody.velocity;
                if (other.transform.position.z == 0)
                {
                    ballVelocity.x *= -1f;
                }
            }
        }
        public void ResetBall()
        {
            mRigidbody.transform.position = new Vector3(0, 0.6f, 0);
            mRigidbody.velocity = Vector3.zero;
            mRigidbody.angularVelocity = Vector3.zero;
        }

        #region testing function
        public void ShootPlayer()
        {
            GameObject player = GameObject.Find("Player");
            //rb.transform.position = new Vector3(0, 1.637092f, 0);
            mRigidbody.velocity = Vector3.zero;
            Vector3 playerLoc = player.transform.position;
            Vector3 movement = playerLoc - transform.position;
            movement.Normalize();
            mRigidbody.velocity = movement * Speed;
        }
        public void FreeShoot()
        {
            Vector3 movement = new Vector3(Xdir, 0, Zdir);
            movement.Normalize();
            mRigidbody.velocity = movement * Speed;
        }
        public void AddHorizontalForce()
        {
            Vector3 force = new Vector3(Xdir, 0, 0);
            force.Normalize();
            mRigidbody.AddForce(force * Speed);
        }  
        public void StopBall()
        {
            mRigidbody.velocity = Vector3.zero;
            mRigidbody.angularVelocity = Vector3.zero;
        }
        #endregion
    }
}
