﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AirHockey
{
    public class BoundaryControl : MonoBehaviour
    {

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                other.GetComponent<PlayerMovement>().ResetPlayer();
            }
            if (other.CompareTag("Enemy"))
            {
                
            }
            if (other.CompareTag("Ball"))
            {
                other.GetComponent<BallMovement>().ResetBall();
            }
        }
    }
}
