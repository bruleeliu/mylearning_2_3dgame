﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AirHockey
{
    public class AI : MonoBehaviour
    {
        [Serializable] public enum AIMode { EnemySide = 1, PlayerSide = -1 };
        [SerializeField] public AIMode WhichSide;
        public bool EnableAI;
        public float Speed = 8f;
        public Rigidbody AirBall;
        public GameState GameStateObject;

        protected Rigidbody mRigidbody;
        protected Vector3 mOriginPosition;
        private enum HitState { NotToHit, ReadyToHit, DoingHit, WaitForFinish, HitFinished, FollowTarget, BackToOrigin };
        private HitState mHitState;
        private bool mInHitProcess;

        protected virtual void Start()
        {
            mHitState = HitState.NotToHit;
            mInHitProcess = false;
            mRigidbody = GetComponent<Rigidbody>();
        }
        protected void AirHockeyAIRun()
        {
            Vector3 ballLoc = AirBall.position;
            float zDir = ballLoc.z * (int)WhichSide;

            if (zDir >= 0.6f)
            {
                //if (mHitState != HitState.DoingHit && mHitState != HitState.HitFinished && mHitState != HitState.WaitForFinish)
                //    mHitState = HitState.ReadyToHit;
                if (!mInHitProcess)
                    mHitState = HitState.ReadyToHit;
            }
            else
            {
                if (zDir > 0)
                {
                    mHitState = HitState.FollowTarget;
                }
                if (zDir < 0)
                {
                    mHitState = HitState.BackToOrigin;
                    ballLoc = mOriginPosition;
                }
            }
            EnemyMovement(mHitState, ballLoc);
        }
        private void EnemyMovement(HitState hitState, Vector3 targetPos)
        {
            Vector3 velocityDirection = Vector3.zero;
            float distance = 0;

            switch (hitState)
            {
                case HitState.HitFinished:
                    mInHitProcess = false;
                    break;
                case HitState.WaitForFinish:
                    break;
                case HitState.DoingHit:
                    StartCoroutine(HitProcessing());
                    mHitState = HitState.WaitForFinish;
                    break;
                case HitState.ReadyToHit:
                    velocityDirection = targetPos - mRigidbody.position;
                    distance = velocityDirection.magnitude;
                    velocityDirection.Normalize();
                    mRigidbody.velocity = velocityDirection * Speed;
                    mRigidbody.AddForce(velocityDirection * distance, ForceMode.Impulse);
                    mHitState = HitState.DoingHit;
                    mInHitProcess = true;
                    break;
                case HitState.FollowTarget:
                    //Follow target X direction
                    distance = Mathf.Abs(targetPos.x - mRigidbody.position.x);
                    //velocityDirection = new Vector3(velocityDirection.x >= 0 ? 1 : -1, 0, 0);
                    velocityDirection.x = (targetPos.x - mRigidbody.position.x) >= 0 ? 1 : -1;
                    if (distance > 0.5f)
                        mRigidbody.velocity = velocityDirection * 0.8f * Speed;
                    else
                        mRigidbody.velocity = velocityDirection * 0.5f;
                    mInHitProcess = false;
                    break;
                case HitState.BackToOrigin:
                    //Back to origin position
                    distance = (targetPos - mRigidbody.position).magnitude; // targetPos = mOriginPosition
                    if (distance > 0.5f)
                    {
                        velocityDirection = targetPos - mRigidbody.position; // targetPos = mOriginPosition
                        velocityDirection.Normalize();
                        mRigidbody.velocity = velocityDirection * Speed;
                    }
                    else
                    {
                        mRigidbody.velocity = Vector3.zero;
                        mInHitProcess = false;
                    }
                    break;
                default:
                    Debug.Log("????");
                    break;
            }

        }

        private IEnumerator HitProcessing()
        {
            int count = 0;
            float distance = 0;
            bool hitted = false;
            HitState tempState = HitState.BackToOrigin;

            while (count < 3)
            {
                if (mInHitProcess == false)
                    break;
                if (Mathf.Approximately(mRigidbody.position.z, float.Epsilon))
                {
                    //mRigidbody.velocity = Vector3.zero;
                    //mRigidbody.angularVelocity = Vector3.zero;
                    tempState = HitState.BackToOrigin;
                    break;
                }
                yield return 0;
                if (mRigidbody.position.z <= AirBall.position.z)
                {
                    tempState = HitState.BackToOrigin;
                    break;
                }
                yield return 0;
                distance = (AirBall.position - mRigidbody.position).magnitude;
                //Debug.Log($"distance={distance}, ball pos={AirBall.position}, my pos={mRigidbody.position}");
                if (distance <= 0.5f)
                    hitted = true;
                yield return 0;
                if (hitted)
                {
                    if (AirBall.velocity.z < 0 && distance > 0.5f)
                        count++;
                }
            }
            mHitState = tempState;
        }
    }
}