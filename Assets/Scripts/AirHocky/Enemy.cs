﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AirHockey
{
    public class Enemy : AI
    {
        protected override void Start()
        {
            mOriginPosition = new Vector3(0, 0.6f, 3.5f);
            base.Start();
        }

        private void FixedUpdate()
        {
            if (!GameStateObject.IsStartingGame())
                return;
            if(EnableAI)
                AirHockeyAIRun();
        }

    }
}