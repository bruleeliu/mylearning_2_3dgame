﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace AirHockey
{
    public class GameState : MonoBehaviour
    {
        public Text CountdownText;
        public Text PlayerScoreText;
        public Text EnemyScoreText;

        private float mCountdownCounter;
        private bool mCountdownEnd;
        private int mPlayerScore;
        private int mEnemyScore;

        void Start()
        {
            mCountdownCounter = 3f;
            CountdownText.text = ((int)mCountdownCounter).ToString();
            mCountdownEnd = false;
            PlayerScoreText.text = "Player : 0";
            EnemyScoreText.text = "Enemy : 0";
            mPlayerScore = 0;
            mEnemyScore = 0;
            //Player.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionZ;
            //AI.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionZ;
            //Ball.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionZ;
        }
        void Update()
        {
            CountdownStart();
        }

        void CountdownStart()
        {
            if (mCountdownEnd == true)
                return;
            if (mCountdownCounter <= 1f)
            {
                CountdownText.text = "";
                mCountdownEnd = true;
                //Player.GetComponent<Rigidbody>().constraints |= RigidbodyConstraints.FreezePositionY;
                //AI.GetComponent<Rigidbody>().constraints |= RigidbodyConstraints.FreezePositionY;
                //Ball.GetComponent<Rigidbody>().constraints |= RigidbodyConstraints.FreezePositionY;
            }
            else
            {
                CountdownText.text = ((int)mCountdownCounter).ToString();
                mCountdownCounter -= Time.deltaTime;
            }
        }

        public bool IsStartingGame()
        {
            return mCountdownEnd;
        }

        public void AddPlayerScore()
        {
            mPlayerScore += 1;
            PlayerScoreText.text = "Player : " + mPlayerScore.ToString();
            //Ball.GetComponent<BallMovement>().ResetBall();
        }

        public void AddEnemyScore()
        {
            mEnemyScore += 1;
            EnemyScoreText.text = "Enemy : " + mEnemyScore.ToString();
            //Ball.GetComponent<BallMovement>().ResetBall();
        }
    }
}