﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AirHockey
{
    public class PlayerMovement : AI
    {
        //public float Speed = 8f;

        //private Rigidbody mRigidbody;

        protected override void Start()
        {
            //mRigidbody = GetComponent<Rigidbody>();
            mOriginPosition = new Vector3(0, 0.6f, -3.5f);
            base.Start();
        }

        private void FixedUpdate()
        {
            if (!GameStateObject.IsStartingGame())
                return;
            if (EnableAI)
                AirHockeyAIRun();
            else
                MovePlayer();
        }

        void MovePlayer()
        {
            float horizontal = Input.GetAxis("Horizontal");
            float veritcal = Input.GetAxis("Vertical");
            Vector3 movement = new Vector3(horizontal, 0, veritcal);
            movement.Normalize();
            if (mRigidbody.position.z >= -0.3f && movement.z > 0)
                movement.z = 0;
            else if (mRigidbody.position.z <= -3.75f && movement.z < 0)
                movement.z = 0;
            if (mRigidbody.position.x >= 1.75f && movement.x > 0)
                movement.x = 0;
            else if (mRigidbody.position.x <= -1.75f && movement.x < 0)
                movement.x = 0;
            mRigidbody.velocity = movement * Speed;
        }

        public void ResetPlayer()
        {
            mRigidbody.transform.position = new Vector3(0, 0.6f, -3.5f);
            mRigidbody.velocity = Vector3.zero;
            mRigidbody.angularVelocity = Vector3.zero;
        }
    }
}