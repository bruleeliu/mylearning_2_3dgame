﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HauntedHouse
{    public class PlayerMovement : MonoBehaviour
    {
        public float TurnSpeed = 20f;

        Vector3 mMovement;
        Animator mAnimator;
        Rigidbody mRigidbody;
        Quaternion mRotation = Quaternion.identity;
        AudioSource mAudioSource;

        // Start is called before the first frame update
        void Start()
        {
            mAnimator = GetComponent<Animator>();
            mRigidbody = GetComponent<Rigidbody>();
            mAudioSource = GetComponent<AudioSource>();
        }

        // Update is called once per frame
        void FixedUpdate()
        {
            MovementMethod1();
        }

        void MovementMethod1() // Move same direction with arrow key you pressed.
        {
            float horizontal = Input.GetAxis("Horizontal");
            float vertical = Input.GetAxis("Vertical");
            mMovement.Set(horizontal, 0f, vertical);
            mMovement.Normalize();
            bool hasHorizontalInput = !Mathf.Approximately(horizontal, 0f);
            bool hasVerticalInput = !Mathf.Approximately(vertical, 0f);
            bool isWalking = hasHorizontalInput || hasVerticalInput;
            mAnimator.SetBool("IsWalking", isWalking);
            if(isWalking)
            {
                if(!mAudioSource.isPlaying)
                    mAudioSource.Play();
            }
            else
            {
                mAudioSource.Stop();
            }
            Vector3 desiredForward = Vector3.RotateTowards(transform.forward, mMovement, TurnSpeed * Time.deltaTime, 0f);
            mRotation = Quaternion.LookRotation(desiredForward);
        }

        private void OnAnimatorMove()
        {
            mRigidbody.MovePosition(mRigidbody.position + mMovement * mAnimator.deltaPosition.magnitude);
            mRigidbody.MoveRotation(mRotation);
        }
    }

}
