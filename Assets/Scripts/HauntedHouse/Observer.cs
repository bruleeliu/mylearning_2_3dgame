﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HauntedHouse
{
    public class Observer : MonoBehaviour
    {
        public Transform Player;
        public GameEnding GameEndingObject;

        bool mIsPlayerInRange;

        private void OnTriggerEnter(Collider other)
        {
            if(other.transform == Player)
            {
                mIsPlayerInRange = true;
            }
        }
        private void OnTriggerExit(Collider other)
        {
            if(other.transform == Player)
            {
                mIsPlayerInRange = false;
            }
        }
        private void Update()
        {
            if(mIsPlayerInRange == true)
            {
                Vector3 direction = Player.position - transform.position + Vector3.up;
                Ray ray = new Ray(transform.position, direction);
                RaycastHit raycastHit;
                if (Physics.Raycast(ray, out raycastHit) == true)
                {
                    if(raycastHit.collider.transform == Player)
                    {
                        GameEndingObject.CaughtPlayer();
                    }    
                }
            }
        }
    }
}