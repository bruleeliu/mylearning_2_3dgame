﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace HauntedHouse
{
    public class WaypointPatrol : MonoBehaviour
    {
        public NavMeshAgent MyNavMeshAgent;
        public Transform[] Waypoints;

        int mCurrentWaypointsIndex;

        private void Start()
        {
            MyNavMeshAgent.SetDestination(Waypoints[0].position);
        }
        private void Update()
        {
            if(MyNavMeshAgent.remainingDistance < MyNavMeshAgent.stoppingDistance)
            {
                mCurrentWaypointsIndex = (mCurrentWaypointsIndex + 1) % Waypoints.Length;
                MyNavMeshAgent.SetDestination(Waypoints[mCurrentWaypointsIndex].position);
            }
        }
    }
}