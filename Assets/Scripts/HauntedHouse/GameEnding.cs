﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace HauntedHouse
{
    public class GameEnding : MonoBehaviour
    {
        public float FadeDuration = 1f;
        public float DisplayImageDuration = 1f;
        public GameObject Player;
        public CanvasGroup ExitBackgroundImageCanvasGroup;
        public CanvasGroup CaughtBackgroundImageCanvasGroup;
        public AudioSource ExitAudio;
        public AudioSource CaughtAudio;

        bool mIsPlayerAtExit;
        bool mIsPlayerCaught;
        float mTimer;
        bool mHasAudioPlayed;
        private void OnTriggerEnter(Collider other)
        {
            if(other.gameObject == Player)
            {
                mIsPlayerAtExit = true;
            }    
        }
        private void Update()
        {
            if(mIsPlayerAtExit == true)
            {
                EndLevel(ExitBackgroundImageCanvasGroup, false, ExitAudio);
            }
            else if (mIsPlayerCaught == true)
            {
                EndLevel(CaughtBackgroundImageCanvasGroup, true, CaughtAudio);
            }
        }

        void EndLevel(CanvasGroup imageCanvasGroup, bool doReset, AudioSource audioSource)
        {
            if(!mHasAudioPlayed)
            {
                audioSource.Play();
                mHasAudioPlayed = true;
            }
            mTimer += Time.deltaTime;
            imageCanvasGroup.alpha = mTimer / FadeDuration;
            if (mTimer > FadeDuration + DisplayImageDuration)
                if (doReset == true)
                {
                    SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
                }
                else
                {
                    Application.Quit();
                }
        }

        public void CaughtPlayer()
        {
            mIsPlayerCaught = true;
        }
    }
}
